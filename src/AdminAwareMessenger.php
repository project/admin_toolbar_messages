<?php

namespace Drupal\admin_toolbar_messages;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\AdminContext;

final class AdminAwareMessenger implements MessengerInterface {

  private const TYPE_PREFIX = 'admin:';

  private MessengerInterface $inner;

  private AdminContext $adminContext;

  public function __construct(MessengerInterface $inner, AdminContext $admin_context) {
    $this->inner = $inner;
    $this->adminContext = $admin_context;
  }

  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE) {
    if ($this->isAdminContext()) {
      return $this->addAdminMessage($message, $type, $repeat);
    }
    return $this->inner->addMessage($message, $type, $repeat);
  }

  public function addAdminMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE): self {
    $this->inner->addMessage($message, self::TYPE_PREFIX . $type, $repeat);
    return $this;
  }

  public function addStatus($message, $repeat = FALSE) {
    return $this->addMessage($message, self::TYPE_STATUS, $repeat);
  }

  public function addError($message, $repeat = FALSE) {
    return $this->addMessage($message, self::TYPE_ERROR, $repeat);
  }

  public function addWarning($message, $repeat = FALSE) {
    return $this->addMessage($message, self::TYPE_WARNING, $repeat);
  }

  public function all(): array {
    $messages = $this->allExceptAdmin();

    // On admin pages return both admin as non-admin messages.
    if ($this->isAdminContext()) {
      foreach ($this->allAdmin() as $type => $type_messages) {
        $messages[$type] = array_merge(
          $type_messages,
          $messages[$type] ?? []
        );
      }
    }

    return $messages;
  }

  public function allAdmin(): array {
    return $this->allPrefixed(TRUE);
  }

  public function allExceptAdmin(): array {
    return $this->allPrefixed(FALSE);
  }

  public function messagesByType($type): array {
    return $this->inner->messagesByType($type);
  }

  public function deleteAll(): array {
    $messages = $this->deleteAllExceptAdmin();

    // On admin pages return both admin as non-admin messages.
    if ($this->isAdminContext()) {
      foreach ($this->deleteAllAdmin() as $type => $type_messages) {
        $messages[$type] = array_merge(
          $type_messages,
          $messages[$type] ?? []
        );
      }
    }

    return $messages;
  }

  public function deleteAllAdmin(): array {
    $messages = $this->allAdmin();

    foreach (array_keys($messages) as $type) {
      $this->inner->deleteByType(self::TYPE_PREFIX . $type);
    }

    return $messages;
  }

  public function deleteAllExceptAdmin(): array {
    $messages = $this->allExceptAdmin();

    foreach (array_keys($messages) as $type) {
      $this->inner->deleteByType($type);
    }

    return $messages;
  }

  public function deleteByType($type): array {
    $messages = $this->inner->deleteByType($type);

    if ($this->isAdminContext()) {
      $messages = array_merge(
        $this->inner->deleteByType(self::TYPE_PREFIX . $type),
        $messages,
      );
    }

    return $messages;
  }

  private function isAdminContext(): bool {
    return $this->adminContext->isAdminRoute();
  }

  private function allPrefixed(bool $prefixed): array {
    $messages = [];

    $prefix_length = strlen(self::TYPE_PREFIX);

    foreach ($this->inner->all() as $type => $type_messages) {
      $is_prefixed = strpos($type, self::TYPE_PREFIX) === 0;
      if ($is_prefixed !== $prefixed) {
        continue;
      }

      if ($is_prefixed) {
        $type = substr($type, $prefix_length);
      }

      $messages[$type] = $type_messages;
    }

    return $messages;
  }

}
