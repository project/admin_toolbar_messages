<?php

namespace Drupal\admin_toolbar_messages;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class AdminToolbarMessagesBuilder implements ContainerInjectionInterface, TrustedCallbackInterface {

  use StringTranslationTrait;

  private AdminAwareMessenger $messenger;

  private AdminContext $adminContext;

  public function __construct(AdminAwareMessenger $messenger, AdminContext $admin_context) {
    $this->messenger = $messenger;
    $this->adminContext = $admin_context;
  }

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('admin_toolbar_messages.messenger'),
      $container->get('router.admin_context')
    );
  }

  public static function trustedCallbacks(): array {
    return ['build'];
  }

  public function build(): array {
    if ($this->adminContext->isAdminRoute()) {
      return [];
    }

    $messages = $this->messenger->deleteAllAdmin();

    if (empty($messages)) {
      return [];
    }

    return [
      '#theme' => 'status_messages__admin_toolbar_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => $this->t('Status message'),
        'error' => $this->t('Error message'),
        'warning' => $this->t('Warning message'),
      ],
    ];
  }

}
